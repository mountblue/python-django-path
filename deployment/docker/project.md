# Docker project

Deploy your Flask project using Docker

Create the following Docker images:

1. Postgres - Expose port 5432
2. Flask - Expose port 8000. (Use gunicorn)
3. Nginx - Expose port 80

Use docker-compose to connect the above containers
