# Deploy `bootcamp` on an EC2 instance


## Part 1.1 - Basic deployment

**Goal**: Get the project running in development mode on EC2

* Install Postgres, Redis, Python3, virtualenv etc. 
* Clone the git repo
* Install the requirements
* Run the migrations
* Create a superuser
* Start the development server

________


## Part 1.2 - Production deployment

**Goal**: Get the project running on production mode

* Install Nginx
* Install gunicorn

* Set `DJANGO_SETTINGS_MODULE` to `config.settings.production`
* Make appropriate changes in `config.settings.production.py`

* Configure Nginx so that it forwards HTTP requests to gunicorn
* Run gunicorn as a systemd service

___________
