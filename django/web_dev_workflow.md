### Follow the problem-solving approach:

1. Understand the Problem
2. Devise a Plan
3. Carry out the plan
4. Review

**I assume that you've already completed steps 1 and 2. Now it's time to write code.**


_____


### Step 1. Make it work:

#### 1.1: URL / View:

1. Add a url.
2. Create a corresponding view function.
3. Don't write any business logic yet.
4. Return a hello-world response
5. Refresh page. Check if everything works.


#### 1.2: Business logic

1. Open your Django shell - Use `ipython` and `shell_plus`
2. Figure out what ORM queries you need. Use the shell to experiment
3. Create a helper module (you can delete it later if you want to)
4. Define your business logic.
5. Import the helper module. Run your code and see if everything works. Reload the module if you make changes. (`import importlib; reload = importlib.reload`)


#### 1.3: Link business logic to code

1. Import your business logic into your view. 
2. Render template

#### 1.4 API development best practices

Make sure your API / view has all these steps

* Authenticate 
* Handle 404 and other errors
* Authorize - 403
* Validate form - 400
* Business logic - extract into functions if required
* Send response.

____________

### Step 2, 3: Make it readable, modular

1. Place your business logic in one these places: (depending on the codebase)

    - Fat models: models, model managers, querysets
    - Service objects / functions

2. Follow SRP

______


### Step 4: Write tests:

* In fact, you can write tests before you write your code depending on your preference.


______

### Step 5: Make it efficient

* Use Django debug toolbar.
* Make sure you are not making too many SQL queries (n + 1) problem. 
* See if you can use better data structures

_______


### Step 6: Review

* Review your code.
* Make sure you've follow best practices: and go back to previous step if required.



