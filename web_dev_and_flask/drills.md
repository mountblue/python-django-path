## Drills - Web development with Flask

We'll use StackOverflow as an example

### Task - Initialize the project

* Create a directory
* Add a gitignore
* Initialize the git repo
* Create a Flask app
* Set up environment variables 
    - FLASK_DEBUG=1
* Set up connection to database
_________________

### Task - Create the following models with the following fields

* Users can ask questions
* Each Question can have many answers
* Each question will also have one or more associated with it.

```
User
    id - integer, primary key
    username - string, unique

Question
    id - integer, primary key
    title - string
    description - string
    user_id - integer, foreign key to user

Answer
    id - integer, primary key
    body - string
    question_id - integer, foreign key to question
    user_id - integer, foreign key to user

Tag
    id - integer, primary key
    name - string, unique

QuestionTag
    id - integer, primary key
    question_id - integer, foreign key to question
    tag_id - integer, foreign key to tag

```

* Create the above models in a `models.py` file
* Initialize the database
* Migrate the database
_____________

### Task - Create some sample data using Flask shell

* Create 10 Questions. 
* Add 2 to 3 answers to each Question
* Add 2 to 4 tags to each question

(Don't enter each row manually. Instead write a script which inserts random data)

______________

### Drill - SQLAlchemy

Using the Flask shell write SQL queries for the following:

* Fetch question having id = 1
* Fetch questions with following ids - [1, 2, 3] in one query
* Fetch question having id=1 along with all answers belonging to that question in one query
* Fetch all questions belonging to a particular tag

______________


### Drill  - CRUD

1. Question Detail - GET

* Create a route function `question_detail` having url `/questions/<question_id>`
* Render question details - Question title, description, answers, tags


2. Question List:

* Create a route function `question_list` having url `/questions`
* Render all questions along with usernames of each question's asker
* In the templateUse url_for to link to each question's detail page

3. url_for

* What are the arguments accepted by `url_for`. What does it return?
* Use the flask_shell and call url_for with various parameters.

4. Use the Flask shell and list all the routes

4. Question Detail - POST

* Create a route function `create_question` having url `/questions/new`
* When user makes a GET request, show a form having following fields - question title, question description, tags (tags is an input field. Multiple tags can be entered separated by commas)
* When user submits a form, a POST request is made. Save the question details
* Redirect user to question_detail page

5. Chrome Dev Tools

* Open the Network tab. Use all the features you've implemented so far. Understand every request and response header, form data, status_code etc. 

_________

### Visual Debugger

* Learn to use PyCharm or VSCode's debugger.
* Put breakpoints.
* Learn to use step out, step over, step in
* Inspect all the variables
* Learn what a call stack is. 

_______________

### Read Only mode

Imagine your servers are under heavy load!! So you want to put your website in read-only mode, i.e. users can only read existing content, but can't create questions/answers etc. 

How will you do that by writing the minimum possible, elegant code?

________________

### Cookies

* Log in to your project
* Fetch the user_id manually from the session_cookie
* Set a custom cookie called as "hello" and set its value to "world" (Observe Chrome dev tools)