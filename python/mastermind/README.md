## Mastermind


#### Implement a command-line version of the **Mastermind** game in Python

The game is also known as 'Bulls and Cows'

Learn more about the rules at http://5ko.free.fr/en/bk.html


__________

#### Gameplay

1. The program should generate a random 5 digit number as the `answer`. But not reveal it to the player. 
2. The player then guesses a number. The program should accept the number as input from the command line, compare it with the `answer`, and print how many numbers are correct, and also how many numbers were in the correct position.
3. Repeat step 2. The program should give a maximum of 10 chances to the player.
4. If the player wins, the program should give a success message.
5. If the player does not guess the answer even after 10 chances, then the program should give an error message along with the correct answer.
6. The program should reset and start a new game after the previous game has ended.

_____________

#### Code

* Use the `Mastermind` class

```
class MasterMind:
    def __init__(self):
        self._answer = 30330

    def guess(self, number):
        return {'is_correct': False, 'cows': 2, 'bulls': 4}

    def reset(self):
        pass

def test_mastermind():
    m = Mastermind()
    m._answer = 10101

    result = m.guess(10101)
    if result['is_correct'] is False:
        print("Wrong")
    answer = 44444
    answer = 0
    answer = 99999
    answer = 12345
    answer = 50000
```

Feel free to also add other methods to `Mastermind` and other functions outside the `Mastermind` class

```
10000

Too Low, 0 Total Correct, 0 Numbers in correct position

50234

Too Low, 1 total correct, 0 in correct position


75234

Too high, 2 total correct, 1 in correct position


61345

Too Low, 1 total correct, 1 in wrong positiion

75896

Too high, 3 total correct, 2 in correct position


Answer: 72876
10 attempts
```

## Coding Standards

* Check if your code follows PEP8 standards using using http://pep8online.com


## Common Checklist

* Is there a `README.md` file that describes who to install and run the program?
* Presentation: Does the code follow `PEP8` standards?
* Is there a class called `Mastermind`?
* Modularity: Are methods in  `Mastermind` testable? i.e. do they return values that can be tested?
* Readability: Is the code readable? Does it have descriptive variable and method names?
* Modularity: Are methods in `Mastermind` modular?
* Correctness: Does `Mastermind` class have test cases that check edge cases and 'normal' cases?
* Usability: Does the program print understandable messages to the User?

