**REST**

**1. Read the following articles:**

* https://www.quora.com/What-is-a-REST-API

* https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

* https://phauer.com/2015/restful-api-design-best-practices/

* https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9



**References**

* https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api

* https://github.com/microsoft/api-guidelines


____________

**2. Analyze APIs provided by popular websites like StackExchange** for inspiration - https://api.stackexchange.com/docs

In particular experiment with 

* `question-by-id:` http://api.stackexchange.com/docs/questions-by-ids
* `answers-on-questions:` http://api.stackexchange.com/docs/answers-on-questions
* `tags:` http://api.stackexchange.com/docs/tags

For example, to see details about "What does the yield keyword do" at https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do

Paste the `question_id`, 231767 in the API explorer: 

http://api.stackexchange.com/docs/questions-by-ids#order=desc&sort=activity&ids=231767&filter=default&site=stackoverflow&run=true

Also analyze the JSON structure of the response,

http://api.stackexchange.com/questions/231767?order=desc&sort=activity&filter=default&site=stackoverflow

```
{
    "items": [{
        "tags": ["python", "iterator", "generator", "yield", "coroutine"],
        "owner": {
            "reputation": 51279,
            "user_id": 18300,
            "user_type": "registered",
            "accept_rate": 92,
            "profile_image": "https://i.stack.imgur.com/jcyI4.jpg?s=128&g=1",
            "display_name": "Alex. S.",
            "link": "https://stackoverflow.com/users/18300/alex-s"
        },
        "is_answered": true,
        "view_count": 1930442,
        "protected_date": 1360547307,
        "accepted_answer_id": 231855,
        "answer_count": 39,
        "score": 8882,
        "last_activity_date": 1548322799,
        "creation_date": 1224800471,
        "last_edit_date": 1513279312,
        "question_id": 231767,
        "link": "https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do",
        "title": "What does the &quot;yield&quot; keyword do?"
    }],
    "has_more": false,
    "quota_max": 300,
    "quota_remaining": 271
}
```

____________

**Go through the DRF tutorial**

http://www.django-rest-framework.org/#tutorial

**Complete the project**

**Once you've implemented the API,**

* Also read a few StackExchange questions on REST API design - https://softwareengineering.stackexchange.com/questions/tagged/rest

* Read https://codewords.recurse.com/issues/five/what-restful-actually-means to get a deeper understanding of what RESTful really means. 
